#!/usr/bin/env ruby
require 'gitlab'

if ARGV.length < 1 then
  STDERR.puts "Usage: $0 <repo_name on Alioth>"
  exit(1)
end
repo = ARGV[0]

Gitlab.endpoint = 'https://salsa.debian.org/api/v4'
Gitlab.private_token = ENV['SALSA_TOKEN']

p = Gitlab.project("med-team/#{repo}")
if p then
  repoid = p.id
  puts "#{repo} -> #{repoid}"
else
  STDERR.puts "no project ID found for #{repo}"
  exit 1
end

puts "deleting #{repo}..."
Gitlab.delete_project(repoid)

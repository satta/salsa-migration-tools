#!/usr/bin/env ruby
require 'gitlab'

if ARGV.length < 2 then
  STDERR.puts "Usage: $0 <teamname> <projectname>"
  STDERR.puts "     e.g. med-team/foobar"
  exit 1
end
namespace = ARGV[0]
repo = ARGV[1]

Gitlab.endpoint = 'https://salsa.debian.org/api/v4'
if ENV['SALSA_TOKEN']
  Gitlab.private_token = ENV['SALSA_TOKEN']
else
  STDERR.puts "no token set in environment -- set SALSA_TOKEN to use this script"
  exit 1
end

# check if project already exists
begin
  p = Gitlab.project("#{namespace}/#{repo}")
  STDERR.puts "path #{namespace}/#{repo} already present, aborting"
  exit 1
rescue
  # pass
end

# check if namespace exists
nsid = nil
Gitlab.namespaces().each do |ns|
  if ns.name == namespace then
    nsid = ns.id
  end
end
if nsid == nil then
  STDERR.puts "namespace #{namespace} not found or not editable, aborting"
  exit 1
end

begin
  p = Gitlab.create_project(repo, { namespace_id: "#{nsid}",
                                    visibility: "public",
                                    description: "Debian packaging for #{repo}" })
rescue => e
  STDERR.puts e.to_s
  exit 1
end

puts "Created new repository:"
puts "SSH clone URL:     #{p.ssh_url_to_repo}"
puts "HTTPS clone URL:   #{p.http_url_to_repo}"
puts "Web interface URL: #{p.web_url}"

puts "configuring emails on push for #{repo}..."
Gitlab.change_service(p.id, :emails_on_push, { recipients: 'dispatch@tracker.debian.org debian-med-commit@lists.alioth.debian.org' })
puts "setting IRC notifications for #{repo}..."
Gitlab.change_service(p.id, :irker, { recipients: '#debian-med', server_host: "ruprecht.snow-crash.org" })
puts "setting tagpending hook for #{repo}..."
Gitlab.add_project_hook(p.id, "https://webhook.salsa.debian.org/tagpending/#{repo}", { push_events: true, enable_ssl_verification: true })

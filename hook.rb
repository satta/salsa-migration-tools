#!/usr/bin/env ruby
require 'net/scp'
require 'tempfile'

if ARGV.length < 1 then
  STDERR.puts "Usage: $0 <repo_name on Alioth>"
  exit(1)
end

repo = ARGV[0]

ALIOTH_HOST="git.debian.org"
USERNAME="satta"

DECLINEHOOK=<<~EOF
#!/bin/bash

echo "This repository is disabled for pushes."
echo "Please use the corresponding new repository on Salsa:"
echo ""
echo "HTTPS: https://salsa.debian.org/med-team/#{repo}.git or"
echo "SSH: git@salsa.debian.org:med-team/#{repo}.git"
exit 1
EOF

hook_file = Tempfile.new(repo)
hook_file.write(DECLINEHOOK)
hook_file.close
hook_filename = "/git/debian-med/#{repo}.git/hooks/pre-receive"
Net::SCP.upload!(ALIOTH_HOST, USERNAME, hook_file.path, hook_filename)
Net::SSH.start(ALIOTH_HOST, USERNAME) do |ssh|
  result = ssh.exec!("chmod +rx #{hook_filename}")
end

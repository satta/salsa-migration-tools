#!/usr/bin/env ruby
require 'gitlab'

if ARGV.length < 1 then
  STDERR.puts "Usage: $0 <repo_name on Alioth>"
  exit(1)
end
repo = ARGV[0]

Gitlab.endpoint = 'https://salsa.debian.org/api/v4'
Gitlab.private_token = 'REDACTED'

p = Gitlab.project("med-team/#{repo}")
if p then
  repoid = p.id
  puts "#{repo} -> #{repoid}"
else
  STDERR.puts "no project ID found for #{repo}"
  exit 1
end

puts "setting emails on push for #{repo}..."
Gitlab.change_service(repoid, :emails_on_push, { recipients: 'dispatch@tracker.debian.org debian-med-commit@lists.alioth.debian.org' })

puts "setting IRC notifications for #{repo}..."
Gitlab.change_service(repoid, :irker, { recipients: '#debian-med', server_host: "ruprecht.snow-crash.org" })

puts "setting tagpending hook for #{repo}..."
Gitlab.add_project_hook(repoid, "https://webhook.salsa.debian.org/tagpending/#{repo}", { push_events: true, enable_ssl_verification: true })

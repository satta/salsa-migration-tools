
# Debian Med Salsa Tools

These are some scripts to help with migrating and converting project repositories on Salsa:

- `salsa_import.rb` -- to import a whole project from Debian Med's old Alioth repo space into the Salsa team space
- `hook.rb` -- to set pre-receive hooks locking the source repo (this is also done in the script above, but can be done explicitly for existing repos using this script)
- `integrations.rb` -- sets email-on-push and IRC notifications for the given Debian Med project (emails are sent to `dispatch@tracker.debian.org` and the traditional `debian-med-commit@lists.alioth.debian.org`)
- `redirect_list.rb` -- prepares a list of mappings suitable for AliothRewriter
- `setup-repository.rb` -- creates a new empty repository in a given team space, with Debian Med specific integration services enabled

## Prerequisites

We need the Ruby gitlab gem:
```
apt install ruby-gitlab
```
This will also pull in a Ruby interpreter if not present already.

## Authentication

GitLab private tokens can be set in the scripts themselves, with the exception of `setup-repository.rb`. In the latter one would need to set an environment variable with the token:
```
export SALSA_TOKEN=abcdefghijklmnop
```

## License

MIT

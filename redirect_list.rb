#!/usr/bin/env ruby
require 'gitlab'
require 'net/scp'
require 'tempfile'

ALIOTH_HOST="git.debian.org"
USERNAME="satta"

Gitlab.endpoint = 'https://salsa.debian.org/api/v4'
Gitlab.private_token = 'REDACTED'

projects = []

1.upto(13) do |n|
 projects += Gitlab.group_projects(2799, { per_page: 100, page: n })
end

puts projects.length
projects.each do |p|
  puts "debian-med/#{p.name} med-team/#{p.name}"
end
